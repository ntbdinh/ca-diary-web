import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  width: 300px;
  height: max-content;
  padding: 0px 0 0;
  margin: auto;
`;

const Container = styled.div`
  height: 240px;
  width: 250px;
  margin: 0 0 100px -40px;
  padding: 20px 45px 50px 80px;
  text-align: center;
  height: 300px;
`;

function Success() {
  return (
    <Wrapper>
      <Container>
        <div>
          <h1>Chúc mừng pé iu !!!</h1>
        </div>
      </Container>
    </Wrapper>
  );
}

export default Success;
