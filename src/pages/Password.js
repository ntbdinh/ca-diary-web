import React from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Button from '../components/Button';
import Link from '../components/Link';
import TextMessage from '../components/TextMessage';

const Wrapper = styled.div`
  width: 300px;
  padding: 20px 0 40px;
  margin: auto;

  input {
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    border-radius: 5px;
    margin: 0 0 15px;
    padding: 6px;
    box-sizing: border-box;
    font-size: 1.05rem;
  }
`;

const Container = styled.div`
  text-align: center;
`;

function Password({ history }) {
  return (
    <Wrapper>
      <Container>
        <div>
          <h1>Quên mật khẩu?</h1>
          <h3>Lấy lại mật khẩu tại đây.</h3>
          <input type="email" placeholder=" Email"/>
          <Button onClick={() => history.push('/success')}>Lấy lại mật khẩu</Button>
          <TextMessage>Đã có tài khoản? <Link to="/login">Đăng nhập</Link></TextMessage>
          <TextMessage>Chưa đăng ký? <Link to="/reg">Tạo tài khoản</Link></TextMessage>
        </div>
      </Container>
    </Wrapper>
  );
}

export default withRouter(Password);
