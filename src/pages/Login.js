import React from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Button from '../components/Button';
import Link from '../components/Link';
import TextMessage from '../components/TextMessage';

const Wrapper = styled.div`
  width: 300px;
  padding: 20px 0 40px;
  margin: auto;
  
  input {
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    border-radius: 5px;
    margin: 0 0 15px;
    padding: 6px;
    box-sizing: border-box;
    font-size: 1.05rem;
  }
`;

const Container = styled.div`
  background-image: url(${require('../images/StickyNote.png')});
  background-size: cover;
  background-position: center;
  height: 240px;
  width: 250px;
  margin-left: -40px;
  padding: 40px 45px 50px 80px;
  text-align: center;
`;

const LoginContainer = styled.div`
  padding: 25px 0px 15px;
`;

function Login({ history }) {
  return (
    <Wrapper>
      <Container>
        <LoginContainer>
          <input type="text" placeholder=" Email"/>
          <input type="password" placeholder=" Mật khẩu "/>
          <Button
            onClick={() => history.push('/')}>Đăng nhập</Button>
          <TextMessage>Chưa đăng ký? <Link to="/reg">Tạo tài khoản</Link></TextMessage>
          <TextMessage> <Link to="/psw">Quên mật khẩu</Link></TextMessage>
        </LoginContainer>
      </Container>
    </Wrapper>
  );
}

export default withRouter(Login);
