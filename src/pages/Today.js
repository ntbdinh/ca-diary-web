import React from 'react';
import Cover from '../images/cover.jpg';

function Today() {
  return (
    <>
    <img src={Cover} style={{width: '100%'}} alt="cover"/>
    </>
  );
}

export default Today;
