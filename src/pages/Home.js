import React from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Cover from '../images/cover.jpg';
import CommonButton from '../components/Button';
import colors from '../utils/colors';

const Wrapper = styled.div`
  padding: 20px;
`;

const CoverImg = styled.img`
  width: 100%;
  max-height: 500px;
  object-fit: cover;
  border-radius: 20px;
`;

const Container = styled.div`
  padding: 50px 0;
`;

const Button = styled(CommonButton)`
  height: 150px;
  width: 400px;
  font-size: 1.75rem;
  border-radius: 10px;
  text-transform: none;
  color: ${colors.primaryColor};
  background: ${colors.homeButtonBackgroundColor};
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 900px;
  margin: auto;
  
  ${Button} + ${Button} {
    margin-left: 5px;
  }
`;

const Quote = styled.p`
  text-align: center;
  font-size: 1.75rem;
  margin-bottom: 0;
`;

function Home({ history }) {
  return (
    <Wrapper>
      <CoverImg src={Cover} />
      <Container>
        <ButtonContainer>
          <Button onClick={() => history.push('/today')}>Hôm nay thế nào?</Button>
          <Button onClick={() => history.push('/yesterday')}>Hôm qua thì sao?</Button>
        </ButtonContainer>
        <br/>
        <br/>
        <Quote>
          Hạnh phúc là không chờ đợi, bạn phải nắm bắt hoặc tự mình tạo ra nó.
        </Quote>
      </Container>
    </Wrapper>
  );
}

export default withRouter(Home);
