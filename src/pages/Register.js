import React from 'react';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import Button from '../components/Button';
import Link from '../components/Link';
import TextMessage from '../components/TextMessage';

const Wrapper = styled.div`
  width: 300px;
  padding: 20px 0 40px;
  margin: auto;
  
  input {
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    border-radius: 5px;
    margin: 0 0 15px;
    padding: 6px;
    box-sizing: border-box;
    font-size: 1.05rem;
  }
`;

const Container = styled.div`
  text-align: center;
`;

function Register({ history }) {
  return (
    <Wrapper>
      <Container>
        <h1>Đăng ký</h1>
        <input type="name" placeholder=" Tên"/>
        <input type="email" placeholder=" Địa chỉ email"/>
        <input type="password" placeholder=" Mật khẩu"/>
        <input type="password" placeholder=" Nhập lại mật khẩu"/>
        <input align="left" name="agreement" type="radio"/>Đồng ý các điều khoản.
        <br/>
        <br/>
        <Button onClick={() => history.push('/success')}>Đăng ký</Button>
        <TextMessage>Đã có tài khoản? <Link to="/login">Đăng nhập</Link></TextMessage>
      </Container>
    </Wrapper>
  );
}

export default withRouter(Register);
