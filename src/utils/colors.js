export default {
  primaryColor: '#34211e',
  secondaryColor: '#f4e3c5',
  tertiaryColor: '#b77a4d',
  tertiaryColor50: 'rgba(183,122,77,0.5)',
  quaternaryColor: '#7a2f20',
  quinaryColor: '#183345',
  senaryColor: '#36546b',
  buttonColor: '#5d2a08',
  homeButtonBackgroundColor: '#dcab83',
  textMessageColor: '#5d5757'
}
