import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Home from './pages/Home';
import Today from './pages/Today';
import Yesterday from './pages/Yesterday';
import Login from './pages/Login';
import Password from './pages/Password';
import Register from './pages/Register';
import Success from './pages/Success';
import Background from './components/PageLayout/Background'
import Layout from './components/PageLayout/Layout'
import Header from './components/PageLayout/Header'
import Content from './components/PageLayout/Content'

function App() {
  return (
    <Router>
      <Background>
        <Layout>
          <Header/>

          <Content>
              <Switch>
                <Route path="/yesterday">
                  <Yesterday />
                </Route>
                <Route path="/today">
                  <Today />
                </Route>
                <Route path="/login">
                  <Login />
                </Route>
                <Route path="/psw">
                  <Password />
                </Route>
                <Route path="/reg">
                  <Register />
                </Route>
                <Route path="/success">
                  <Success />
                </Route>
                <Route path="/">
                  <Home />
                </Route>
              </Switch>
          </Content>
        </Layout>
      </Background>
    </Router>
  );
}

export default App;
