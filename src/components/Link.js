import { Link as RouterLink } from 'react-router-dom';
import styled from 'styled-components';
import colors from '../utils/colors';

const Link = styled(RouterLink)`
  color: ${colors.primaryColor};
  text-decoration: solid;
  transition: all 0.3s ease;
  cursor: pointer;
  margin: -5px;
  padding: 5px;
  border-radius: 4px;
  
  :hover {
    background: ${colors.tertiaryColor50};
    color: ${colors.primaryColor};
  }
`;

export default Link;
