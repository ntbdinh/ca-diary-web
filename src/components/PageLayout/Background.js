import React from 'react';
import styled from 'styled-components';
import colors from '../../utils/colors';

const Wrapper = styled.div`
  background: ${colors.quinaryColor};
  background-size: cover;
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
`;

function Background({ children }) {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  );
}

export default Background;
