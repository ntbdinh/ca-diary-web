import React from 'react';
import styled from 'styled-components';
import colors from '../../utils/colors';

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  overflow: auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: start;
`;

const Container = styled.div`
  max-width: 1024px;
  width: 100%;
  position: relative;
  box-shadow: rgba(0,0,0,0.35) 24px 24px 40px 15px;
  background: ${colors.secondaryColor};
`;

function Layout({ children }) {
  return (
    <Wrapper>
      <Container>{children}</Container>
    </Wrapper>
  );
}

export default Layout;
