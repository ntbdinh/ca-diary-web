import React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';
import Logo from '../../images/logo.png';
import colors from '../../utils/colors';

const Wrapper = styled.div`
  background: ${colors.primaryColor};
  height: 50px;
  display: flex;
  padding: 0 16px 0 16px;
  justify-content: space-between;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
`;

const LogoImg = styled.img`
  width: 50px;
  height: 50px;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
  border-radius: 4px;
  padding-right: 5px;

  :hover {
    background: ${colors.tertiaryColor50};
  }
`;

const LoginButton = styled(Link)`
  height: 40px;
  background: transparent;
  border-color: transparent;
  color: ${colors.secondaryColor};
  font-size: 1.25rem;
  display: flex;
  align-items: center;
  text-decoration: none;
  -webkit-transition: all 0.3s ease;
  transition: all 0.3s ease;
  border-radius: 4px;
  padding: 0 16px;
  margin: 5px 0;
  
  :hover {
    background: ${colors.tertiaryColor50};
  }
`;

function Header() {
  return (
      <Wrapper>
        <Link to="/">
          <LogoImg src={Logo} />
        </Link>
        <LoginButton to="/login">Đăng nhập</LoginButton>
      </Wrapper>
  );
}

export default Header;
