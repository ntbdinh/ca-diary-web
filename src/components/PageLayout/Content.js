import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  padding-top: 50px;
  min-height: 400px;
`;

function Content({ children }) {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  );
}

export default Content;
