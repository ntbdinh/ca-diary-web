import styled from 'styled-components';
import colors from '../utils/colors';

const Button = styled.button`
  text-transform: uppercase;
  outline: 0;
  background: ${colors.quaternaryColor};
  width: 100%;
  border: 0;
  border-radius: 5px;
  padding: 15px;
  color: ${colors.secondaryColor};
  font-size: 14px;
  transition: all 0.3s ease;
  cursor: pointer;
  
  :hover {
    background: ${colors.tertiaryColor};
    color: ${colors.primaryColor};
  }
`;

export default Button;
