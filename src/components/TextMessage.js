import styled from 'styled-components';
import colors from '../utils/colors';

const TextMessage = styled.p`
  margin: 15px 0 0;
  color: ${colors.textMessageColor};
  font-size: 1rem;
`;

export default TextMessage;
